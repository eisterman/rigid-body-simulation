extern crate nalgebra as na;
extern crate ncollide3d;
extern crate nphysics3d;
extern crate nphysics_testbed3d;
extern crate gnuplot;

use std::sync::{Arc, Mutex};
use na::{Isometry3, Point3, Vector3};
use nphysics3d::math::AngularVector;
use ncollide3d::shape::{Cuboid, ShapeHandle};
use nphysics3d::world::World;
use nphysics3d::object::Material; //BodyHandle
use nphysics3d::volumetric::Volumetric;
use nphysics_testbed3d::Testbed;

const COLLIDER_MARGIN: f32 = 0.01;

fn main() {
    /*
     * World
     */
    let mut world = World::new();
    //world.set_gravity(Vector3::new(0.0, -9.81, 0.0));

    // Material for all objects.
    let material = Material::default();

    /*
     * Ground.
     *
    let ground_size = 50.0;
    let ground_shape =
        ShapeHandle::new(Cuboid::new(Vector3::repeat(ground_size - COLLIDER_MARGIN)));
    let ground_pos = Isometry3::new(Vector3::y() * -ground_size, na::zero());

    world.add_collider(
        COLLIDER_MARGIN,
        ground_shape,
        BodyHandle::ground(),
        ground_pos,
        material.clone(),
    );*/

    /*
     * Create the solid volume
     */
    let geom = ShapeHandle::new(Cuboid::new(Vector3::new(7.,0.5,5.)));
    let inertia = geom.inertia(0.01);
    let center_of_mass = geom.center_of_mass();

    /*
     * Create the rigid body.
     */
    let pos = Isometry3::new(Vector3::new(0.,0.,0.), na::zero());
    let handle = world.add_rigid_body(pos, inertia, center_of_mass);
    world.rigid_body_mut(handle).unwrap().set_angular_velocity(AngularVector::new(0.,0.,10.));

    /*
     * Create the collider.
     */
    world.add_collider(
        COLLIDER_MARGIN,
        geom.clone(),
        handle,
        Isometry3::identity(),
        material.clone(),
    );

    /*
     * Set up the testbed.
     */
    world.set_timestep(1e-4_f32); //-6 o -4 (non preciso)
    let mut testbed = Testbed::new(world);
    testbed.hide_performance_counters();
    testbed.set_number_of_steps_per_frame(100);
    testbed.look_at(Point3::new(-20.0, 20.0, -20.0), Point3::origin());
    /*testbed.add_callback(move |_world, time| { 
        println!("Callback Time: {}s", time);
    });*/
    let handle_clone = handle.clone();
    let timevec = Arc::new(Mutex::new(Vec::<f32>::new()));
    let timedata = timevec.clone();
    //let velang = Arc::new(Mutex::new(Vec::<AngularVector<f32>>::new()));
    //let vedata = velang.clone();
    let angmom = Arc::new(Mutex::new(Vec::<Vector3<f32>>::new()));
    let angmomdata = angmom.clone();
    let energy = Arc::new(Mutex::new(Vec::<f32>::new()));
    let energydata = energy.clone();
    testbed.add_callback(move |world: &mut World<f32>, time: f32| { 
        let body = world.rigid_body_mut(handle_clone).unwrap();
        let osc = 0.1*(-1_f32).powi((time*10000.) as i32 % 2);
        let angular0 = body.velocity().angular.clone();
        if 1. < time && time < 1.1 {
            body.set_angular_velocity(angular0 + Vector3::new(osc,osc,osc)); //osc,osc,0
        } else {
            timedata.lock().unwrap().push(time);
            //velang.lock().unwrap().push(angular0);
            angmom.lock().unwrap().push(body.inertia().angular * angular0);
            energy.lock().unwrap().push((angular0.transpose() * (body.inertia().angular * angular0)).determinant());
        }
    });
    testbed.run();
    // Exit Operations (Plots)
    use gnuplot::{Figure, Caption, Color};
    // AngMom X
    let y1: Vec<f32> = angmomdata.lock().unwrap().iter().map(|x| x.x).collect();
    let x1: Vec<f32> = timevec.lock().unwrap().iter().map(|x| *x).collect();
    let mut fg1 = Figure::new();
    fg1.axes2d().lines(&x1, y1, &[Caption("X Angular Momentum"), Color("black")]);
    fg1.show();
    // AngMom Y
    let y2: Vec<f32> = angmomdata.lock().unwrap().iter().map(|x| x.y).collect();
    let x2: Vec<f32> = x1;
    let mut fg2 = Figure::new();
    fg2.axes2d().lines(&x2, y2, &[Caption("Y Angular Momentum"), Color("black")]);
    fg2.show();
    // AngMom Z
    let y3: Vec<f32> = angmomdata.lock().unwrap().iter().map(|x| x.z).collect();
    let x3: Vec<f32> = x2;
    let mut fg3 = Figure::new();
    fg3.axes2d().lines(&x3, y3, &[Caption("Z Angular Momentum"), Color("black")]);
    fg3.show();
    // AngMom module
    let y4: Vec<f32> = angmomdata.lock().unwrap().iter().map(|m| (m.x.powi(2) + m.y.powi(2) + m.z.powi(2)).sqrt() ).collect();
    let x4: Vec<f32> = x3;
    let mut fg4 = Figure::new();
    fg4.axes2d().lines(&x4, y4, &[Caption("Angular Momentum Module"), Color("red")]);
    fg4.show();
    // RotEnergy
    let y5: Vec<f32> = energydata.lock().unwrap().iter().map(|m| *m ).collect();
    let x5: Vec<f32> = x4;
    let mut fg5 = Figure::new();
    fg5.axes2d().lines(&x5, y5, &[Caption("Rotational Energy"), Color("blue")]);
    fg5.show();
}